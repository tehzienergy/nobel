if ($(".input--datepicker")[0]){
  $(function() {
    $('.input--datepicker').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
    });
  });
}
