$('.fancy-slider__content').on('init', function() {
  $('.slick-active').prev().addClass('nobless--prev');
})

$('.fancy-slider__content').slick({
  centerMode: true,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        dots: true
      }
    }
  ]
}).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  if (Math.abs(nextSlide - currentSlide) == 1) {
    direction = (nextSlide - currentSlide > 0) ? "right" : "left";
  }
  else {
    direction = (nextSlide - currentSlide > 0) ? "left" : "right";
  }
  
  if (direction == 'left') {
    $('.slick-slide').removeClass('nobless--prev');
    $('.slick-slide.slick-active').prev().prev().addClass('nobless--prev');
  }
  
  else if (direction == 'right') {
    $('.slick-slide').removeClass('nobless--prev');
    $('.slick-slide.slick-active').addClass('nobless--prev');
  }
});
